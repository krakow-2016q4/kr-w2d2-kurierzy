package com.szkolenie.kurier;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        // 1. Wyswietlenie wszystkich paczek niedostarczonych
        for (Package pack : DB.getPackages()) {
            if (pack.getStatus() != Status.DELIVERED) {
                System.out.println(pack);
            }
        }
        System.out.println();

        // 2. Dla kazdego kuriera pokaz paczki niedostarczone
        List<Package> packages = DB.getPackages();
        for (Courier courier : DB.getCouriers()) {
            System.out.println("Kurier: " + courier);
            for (Package pack : packages) {
                if (pack.getRegion() == courier.getRegion() &&
                        pack.getStatus() != Status.DELIVERED) {
                    System.out.println("\t" + pack);
                }
            }
        }
    }
}
